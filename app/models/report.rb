class Report < ApplicationRecord
  
  
  before_validation do
    self.abducted_p = 0.00 if self.abducted_p == nil 
    self.not_abducted_p = 100.00 if self.not_abducted_p == nil
  end
  

  validates :abducted_p, presence: true, numericality: true
  validates :not_abducted_p, presence: true, numericality: true
  
  def fetch
    result1 = (Survivor.abducted.to_f / Survivor.total) * 100
    result2 = (Survivor.not_abducted.to_f / Survivor.total) * 100
    self.update(abducted_p: result1, not_abducted_p: result2 )
  end
  
end

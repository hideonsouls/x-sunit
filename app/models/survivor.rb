class Survivor < ApplicationRecord
  before_create  do
    self.abducted = false
    self.votes = 0
    self.vlist = []
  end

  before_validation { self.gender.downcase! }

  validates :name, presence: true

  GENDER = %w[male female].freeze
  validates :gender, presence: true, inclusion: { in: GENDER, message: "Invalid gender"}

  validates :age, presence: true, inclusion: { in: 0..100 , message: "Invalid age"}

  validates :lat, presence: true, numericality: true, inclusion: { in: -100..100, message: "Invalid latitude, out of bounds!" }

  validates :lng, presence: true, numericality: true, inclusion: { in: -100..100, message: "Invalid longitude, out of bounds!"  }

  def Survivor.abducted
    a = Survivor.where(abducted: true).count
  end

  def Survivor.not_abducted
    a = Survivor.where(abducted: false).count
  end

  def Survivor.total
    a = Survivor.all.count
  end
  
  def vote(target)
    unless self.id == target.id  
      unless self.vlist.include? target.id
        self.update(vlist: self.vlist.push(target.id))
        target.update(votes: target.votes+1)
        target.status_update
      else
        self.vlist.delete(target.id)
        self.update(vlist: self.vlist)
        target.update(votes: target.votes-1)
        target.status_update
      end
      result = { votes: target.votes,
                 abducted: target.abducted }
    else
      error = "Can't vote on yourself"
    end
  end
  
  protected
    
    def status_update
      if self.votes>=3 && self.abducted == false
        self.update(abducted: true)
      elsif self.votes<3 && self.abducted == true 
        self.update(abducted: false)
      end
    end

end

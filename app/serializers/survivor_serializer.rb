class SurvivorSerializer < ApplicationSerializer
  attributes :name, :age, :gender, :link, :status, :location, :votelist

  def status
      {votes: object.votes, abducted: object.abducted}
  end

  def location
    {latitude: object.lat, longitude: object.lng}
  end

  def votelist
    object.vlist
  end

  def link
    "localhost:3000/survivors/#{object.id}"
  end

end

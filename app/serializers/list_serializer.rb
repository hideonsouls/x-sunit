class ListSerializer < ApplicationSerializer
  attributes :id, :name, :status

  def status 
    {
      abducted: object.abducted 
    }
  end 
end

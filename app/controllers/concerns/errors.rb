require 'active_support/concern'

module Errors

  def not_found(e)
    render json: { 
      error: {
        type: e.class.name,
        message: e.message, 
        response: '404' 
      } 
    }, status: :not_found
  end

  def unprocessable(e, obj) 
    render json:{ 
      field: obj.as_json,
      error: {
        type: e.class.name,
        message: e.message, 
        response: '422' 
      } 
    }, status: :unprocessable_entity
  end

  def unchanged(e, obj) 
    serial = SurvivorSerializer.new(obj)
    render json:{ 
      fields: serial.location,
      error: {
        type: e.class.name,
        message: e.message, 
        response: '422' 
      } 
    }, status: :unprocessable_entity
  end
end
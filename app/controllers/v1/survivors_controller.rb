class V1::SurvivorsController < ApplicationController
  before_action :set_survivor, only: [:show, :destroy, :select, :vote]
  before_action :set_current, except: [:show, :index]

  # GET /survivors.json
  def index
      @survivors = Survivor.all
      render json: @survivors, each_serializer: SurvivorSerializer
  end

  # GET /survivors/1.json
  def show
    render json: @survivor, serializer: SurvivorSerializer
  end

  # POST /survivors.json
  def create
    @survivor = Survivor.new(survivor_params)
    begin
      @survivor.save!
      render json: @survivor, serializer: SurvivorSerializer , status: :created
    rescue ActiveRecord::RecordInvalid => e
      obj = @survivor.errors
      unprocessable(e, obj)
    end
  end  
  
  # PATCH/PUT /survivors/1.json
  # After creation, only the coordinates can be changed
  def update
    set_current
    begin
      @current.update!(last_position_params)
      render json: @current, serializer: SurvivorSerializer, status: :ok
    rescue ActiveRecord::RecordInvalid => e
      unchanged(e, @current)
    end
  end
  
  # DELETE /survivors/1.json
  def destroy
    begin 
      @survivor.destroy
      render json: { status: "success"}, status: :ok
    rescue ActiveRecord::RecordNotFound => e
      not_found(e)
    end
  end

  def vote
    result = @current.vote(@survivor)
    if result.class == String
      render json: { error: { message: result, status: 403 } }, status: :forbidden
    else
      render json: { 
        target: @survivor.id, 
        name: @survivor.name, 
        success: result }, status: :ok 
    end
  end
  
  # GET /survivors/current
  # GET /survivors/current.json
  def current
    render json: Survivor.find(Current.first.survivor_id), serializer: SurvivorSerializer, status: :ok
  end

  # POST /survivors/1.json
  def select
    begin
      select_current
      render json: Survivor.find(Current.first.survivor_id), status: :ok
    rescue ActiveRecord::RecordNotFound => e
      not_found(e)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survivor
      begin
        @survivor = Survivor.find(params[:id])
      rescue ActiveRecord::RecordNotFound => e
        not_found(e)
      end
    end
    
    def set_current
      @current = Survivor.find(Current.first.survivor_id)
    end

    def select_current
      Current.first.update(survivor_id: @survivor.id)
    end

    def survivor_params
      params.require(:survivor).permit(:name, :age, :gender, :lat, :lng)
    end

    def last_position_params
      params.permit(:lat, :lng)
      last_p = {lat: params[:lat], lng: params[:lng]}
    end

end

class V1::ReportsController < ApplicationController
  before_action :fetch_report
  
  # GET '/reports/abducted'
  def abducted
    render json: { Data: { report: "abducted", value: @report.abducted_p } }
  end

  def not_abducted
    render json: { Data: { report: "not abducted", value: @report.not_abducted_p } }
  end

  def list
    @list = Survivor.order(:name)
    render json: @list, each_serializer: ListSerializer, status: :ok
  end

  private
  def fetch_report
    @report = Report.first
    @report.fetch
  end

end

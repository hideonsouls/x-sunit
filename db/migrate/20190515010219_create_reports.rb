class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.float :abducted_p
      t.float :not_abducted_p

      t.timestamps
    end
  end
end

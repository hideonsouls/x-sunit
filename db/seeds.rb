# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Survivor.all.count == 0
  15.times do
    name = Faker::Games::WorldOfWarcraft.unique.hero
    age = rand(0..100)
    gender = Faker::Gender.binary_type.downcase
    lat = rand(-100..100)
    lng = rand(-100..100)
    Survivor.create(name: name, age: age, gender: gender, lat: lat, lng: lng)
  end
end

Current.first_or_create(survivor_id: Survivor.first.id)

Report.first_or_create()
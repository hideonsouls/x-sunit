Rails.application.routes.draw do
  api_version(:module => "V1", :path => {:value => "v1"}, :default => true) do
    # Survivors
    resources :survivors
    post '/current/:id',       to: "survivors#select",  as: 'survivor_select'                     
    get '/current/',           to: "survivors#current", as: 'survivor_show'               
    put '/vote/:id',           to: "survivors#vote",    as: 'survivor_vote'
    put '/location/:lat/:lng', to: "survivors#update",  as: 'location_update'             
    
    # Reports
    get '/reports/abducted/',    to: "reports#abducted",     as: 'report_abducted'        
    get '/reports/notabducted/', to: "reports#not_abducted", as: 'report_not_abducted'    
    get '/reports/list/',        to: "reports#list",         as: 'report_list'            
  end  
    

end
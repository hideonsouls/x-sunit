﻿# # X-SUNIT (Extraterrestrial Survival Unit)

Greetings, this repo holds the code for a RESTfull~~ish~~ API for **X-SUNIT** backend intership challenge.
The problem revolves around an apocalyptic world scenario, in which humans are being abducted and swapped by **evil** clones.


## X-SUNIT API
Rails API-only application.
Tools used:

 - OS
 -- [Ubuntu 19.04](http://www.releases.ubuntu.com/19.04/)
 - Database
 -- [PostgreSQL 11.3](https://www.postgresql.org/docs/11/index.html) (Ubuntu 11.3-1.pgdg19.04+1)
 - Language
 -- [Ruby 2.6.1](https://www.ruby-lang.org/en/downloads/)
 - Framework
 -- [Rails 5.2.3](https://rubygems.org/gems/rails/versions/5.2.3)
 - Gems<br>
 -- [active_model_serializers 0.10.0](https://rubygems.org/gems/active_model_serializers)  -> for generating a SerializableModel<br>
 -- [pg 1.1.4](https://rubygems.org/gems/pg)   -> Inferfaces Postgres and Rails<br>
 -- [versionist](https://rubygems.org/gems/versionist/versions/2.0.0) -> Used for scoping controllers and routes

Other gems where also used, but weren't as expressive to this application's purpose. 
# Setup 
1.  API
* First, run bundle on application's root
	
		$ bundle install
* Then, create a local database

		$ rails db:create
* Migrate model's tables into the created database

		$ rails db:migrate
* Seed the database with random testing data

		$ rails db:seed
* Now you're set to run the server ('localhost:3000/')

		$ rails server
2. Postman <br>
There's a postman collection that has basic testing requests for the application located at : 
[/x-sunit/postman/x-sunit.postman_collection.json](https://gitlab.com/hideonsouls/x-sunit/blob/master/postman/x-sunit.postman_collection.json)

	Only need to import it into postman and send requests while the server is running.

# Structure
## Models
Only 3 models where implemented to attain the desired result, those where:
### Survivor
 1. Summary:
- Survivor is the main model, it represents the apocalypse survivors.

 2. Attributes:
 - name -> String
 - age -> Integer between 0 and 100
 - gender -> Included in ['male' 'female'] (binary for the sake of simplicity)
 - votes -> Count how many survivors have flagged this survivor as abducted
 - abducted -> Boolean for survivor's state(true if votes >= 3)
 - lat -> Float of survivor's last latitude (-100..100)
 - lnf -> Float of survivor's last longitude (-100..100)
 - vlist -> [] of the survivor's ids that this survivor has voted on (to prevent vote redundancy)
 3. Methods:	
  

	    def  Survivor.abducted
	    #=> returns amount of survivors flagged as abducted
	     
   	    def  Survivor.not_abducted
   	    #=> returns amount of survivors not flagged as abducted
   	    
   	    def  Survivor.total
   	    #=> returns total amount of survivors
   	    
   	    def vote(target)
   	    #=> vote target as abducted using current survivor as voter
   	    #=> note: as evil clones are still amongst us, they can vote as well	
   	        
   	    protected def status_update
   	    #=> sets :abducted value according to votes

### Current

 1. Summary:
 Simple model that holds a survivor_id, that is currently selected as operator
 2. Relationships:
 - belongs_to :survivor
 holds an survivor_id as foreign key for Survivors
 

### Report
1. Summary:
Report is used for providing updated statistics about survivors
2. Attributes:
- abducted_p -> Percentage of survivors flagged as abducted (float)
- not_abducted_p -> Percentage of survivors unflagged as abducted (float)
3. Methods:
 

		 def fetch
		 #=> refreshes both attributes

## Endpoints
### SurvivorsController
- [GET]  survivors#index ->  /survivors  (Render all survivors)

- [GET]  survivors#show -> /survivors/:id (Render a especific survivor)
- [POST]  survivors#create -> /survivors (Create a survivor)<br>
-- obligatory params are: name, age, gender, lat and lng (latitude and longitude)<br>
-- votes, vlist, abducted are initalized as 0, [] and false respectively.
- [PUT/PATCH]  survivors#update -> /location/:lat/:lng -> Updates current survivor's last location(:lat, :lng)<br>
-- Can only change the location<br>
-- Changes current survivor
- [DELETE] survivors#destroy -> /survivors/:id (Deletes a survivor)<br>
-- As survivors wouldn't be able to delete each other and there's always an active survivor, no restrictions were made (Everybody can delete everybody). 
- [GET] survivors#current -> /current/ (Display current survivor)<br>
-- A current survivor is always selected from the start
- [POST]  survivors#select -> /select/:id (Select current survivor)<br>
-- Selects a survivor as operator
- [PUT] survivors#vote -> /vote/:id (Vote on a survivor)<br>
-- **Evil clones** can still vote, even after they are flagged, because they are trying to blend with the environment. Be careful!<br>
-- Uses current survivor to vote on a survivor corresponding to :id<br>
-- Can't vote twice on same survivor, if it votes again, unvote. 


### ReportsController
- [GET]  reports#abducted ->  /reports/abducted (Render abducted survivor's percentage)<br>

- [GET]  reports#notabducted ->  /reports/notabducted (Render not abducted survivor's percentage)<br>
- [GET]  reports#list ->  /reports/list (Render survivor's list ordered alphabetically)<br>
-- Only shows id, name and status.
## Author
- Mathaus Eugênio - Maintainer // [Gitlab](https://gitlab.com/hideonsouls) - [LinkedIn](https://www.linkedin.com/in/mathaus-eug%C3%AAnio-01b065b7/)

## License
This project is licensed under the MIT license, see LICENSE.md file for more details

